
import BooleanGrammar;
import com.github.h0tk3y.betterParse.grammar.parseToEnd

fun main(args: Array<String>) {
    println("Hello, world!")

    //val expr = "a & (b1 -> c1) | a1 & !b | !(a1 -> a2) -> a"
    val expr = "a & (b1 -> c1) | !b1"

    val ast = BooleanGrammar.parseToEnd(expr);

    var normalForm = convertToNormalForm(ast);

    val tableau = createTableau(normalForm)
    println(tableau)

    val interpretation = extractModel(tableau)
    println(interpretation)
}
