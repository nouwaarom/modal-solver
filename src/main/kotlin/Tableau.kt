

class Tableau(formulas: Set<BooleanExpression>) {
    val formulas = formulas
    var subGoals = listOf<Tableau>()
    var choices = listOf<Tableau>()

    fun addChoices(_choices: List<Tableau>) {
        choices = _choices
    }

    fun addSubgoals(goals: List<Tableau>) {
        subGoals = goals
    }

    override fun toString(): String {
        return toString(0)
    }

    fun toString(depth: Int): String {
        var string = formulas.map(fun (exp: BooleanExpression): String { return " ".repeat(depth) + "Subformula: " + exp.toString() }).joinToString("\n")
        string = " ".repeat(depth) + "Tableau node:\n" + string + "\n"

        if (subGoals.size > 0) {
            string += " ".repeat(depth) + "Subgoals: \n" + subGoals.map(fun (t: Tableau): String { return t.toString(depth+1) }).joinToString("\n")
        }

        if (choices.size > 0) {
            string += " ".repeat(depth) + "Choices: \n" + choices.map(fun (t: Tableau): String { return t.toString(depth+1) }).joinToString("\n")
        }

        return string
    }
}

