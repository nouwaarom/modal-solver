
fun createTableau(formula: BooleanExpression): Tableau {
    val root = Tableau(setOf(formula))
    createTableauNodes(root)

    return root
}

private fun createTableauNodes(root: Tableau) {
    var appliedTableauRule = false
    var appliedFormula: BooleanExpression = FALSE

    var newFormulas = mutableListOf<BooleanExpression>()
    for (formula in root.formulas) {
        if (appliedTableauRule) {
            newFormulas.add(formula)
            continue
        }
        if (formula is And) {
            appliedFormula = formula
            appliedTableauRule = true
            continue
        }

        if (formula is Or) {
            appliedFormula = formula
            appliedTableauRule = true
            continue
        }

        newFormulas.add(formula)
    }

    if (false == appliedTableauRule) {
        return
    }

    if (appliedFormula is And) {
        newFormulas.add(appliedFormula.left)
        newFormulas.add(appliedFormula.right)

        var subGoal = Tableau(newFormulas.toSet())
        createTableauNodes(subGoal)

        root.addSubgoals(listOf(subGoal))
    }

    if (appliedFormula is Or) {
        val left = newFormulas.toMutableList()
            left.add(appliedFormula.left)
        val right = newFormulas.toMutableList()
            right.add(appliedFormula.right)

        val leftChoice = Tableau(left.toSet())
        createTableauNodes(leftChoice)
        val rightChoice = Tableau(right.toSet())
        createTableauNodes(rightChoice)

        root.addChoices(listOf(leftChoice, rightChoice))
    }

    return
}