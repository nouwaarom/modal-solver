

fun convertToNormalForm(formula: BooleanExpression) : BooleanExpression {

    if (formula is Impl) {
        return Or(convertToNormalForm(Not(formula.left)), convertToNormalForm(formula.right))
    }

    if (formula is And) {
        return And(convertToNormalForm(formula.left), convertToNormalForm(formula.right))
    }

    if (formula is Or) {
        return Or(convertToNormalForm(formula.left), convertToNormalForm(formula.right))
    }

    if (formula is Not) {
        if (formula.body is Impl) {
            return And(convertToNormalForm(formula.body.left), convertToNormalForm(Not(formula.body.right)))
        }

        if (formula.body is And) {
            return Or(convertToNormalForm(Not(formula.body.left)), convertToNormalForm(Not(formula.body.right)))
        }

        if (formula.body is Or) {
            return And(convertToNormalForm(Not(formula.body.left)), convertToNormalForm(Not(formula.body.right)))
        }

        if (formula.body is Not) {
            return formula.body.body;
        }
    }

    return formula
}