class Interpretation {
    var trueLiterals: MutableList<String> = mutableListOf()
    var falseLiterals: MutableList<String> = mutableListOf()

    fun addTrueLiteral(literal: String) {
        trueLiterals.add(literal)
    }

    fun addFalseLiteral(literal: String) {
        falseLiterals.add(literal)
    }

    override fun toString(): String {
        return "True literals: " + trueLiterals.toString() + "\n" + "False literals: " + falseLiterals.toString() + "\n"
    }
}